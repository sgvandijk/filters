#include "Filter/ParticleFilter/particlefilter.hh"

#include <random>
#include <iostream>
#include <iterator>

using namespace bold;

class WheelSamplerFactory
{
  int index;
  double beta;
  double maxWeight;

  std::function<double()> rnd;

public:
  WheelSamplerFactory()
  {
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0,1);
    rnd = std::bind(distribution, generator);
  }

  ParticleFilter<2>::ParticleSampler operator() (std::vector<ParticleFilter<2>::Particle> const& particles)
  {
    index = rnd() * particles.size();
    beta = 0.0;
    
    
    maxWeight = std::max_element(
      particles.begin(),particles.end(),
      [](ParticleFilter<2>::Particle const& p1, ParticleFilter<2>::Particle const& p2) {
	return p1.second < p2.second;
      })->second;

    return [&]() {
      beta += rnd() * 2 * maxWeight;
      double weight = particles[index].second;
      while (beta > weight) {
	beta -= weight;
	index = (index + 1) % particles.size();
	weight = particles[index].second;
      }
      return particles[index];
    };    
  }
};

void printParticles(std::string const& tag, ParticleFilter<2>* filter)
{
  std::for_each(filter->getParticles().begin(), filter->getParticles().end(),
		[&](std::pair<Eigen::Vector2d,double> const& v){ std::cout << tag << " " << v.first.transpose() << " " << v.second << std::endl; });

  std::cout << std::endl;
}

int main()
{
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0,1);
  auto rnd = std::bind(distribution, generator);

  WheelSamplerFactory wsf;

  ParticleFilter<2>* filter = new ParticleFilter<2>(1000, [&](){ return Eigen::Vector2d(rnd(),rnd()); }, wsf);

//  printParticles("init", filter);
 
  for (unsigned j = 0; j < 10000; ++j)
  {
    for (unsigned i = 0; i < 4; ++i)
      filter->predict([&](Eigen::Vector2d const& state) { return state + Eigen::Vector2d(rnd(),rnd()); });
    
//  printParticles("pred", filter);
    
    for (unsigned i = 0; i < 4; ++i)
      filter->update([](Eigen::Vector2d const& state) { return pow(state.sum(),2); });
    
//  printParticles("updt", filter);
  }

}
