#ifndef BOLD_LINEAROBSERVATIONMODEL_HH
#define BOLD_LINEAROBSERVATIONMODEL_HH

#include "ObservationModel/GaussianObservationModel/gaussianobservationmodel.hh"

#include <Eigen/LU>

namespace bold
{
  template<int DIM>
  class LinearObservationModel : public GaussianObservationModel<DIM>
  {
  public:
    typedef Eigen::Matrix<double,DIM,1> State;

    virtual Eigen::VectorXd operator()(State const& state, Eigen::VectorXd const& observation) const override
    {
      // Most likely observation given state
      return d_observationMatrix.fullPivLu().solve(state);
    }


    void setObservationMatrix(Eigen::MatrixXd const& matrix)
    {
      assert(matrix.rows() == DIM);
      d_observationMatrix = matrix;
    }

    Eigen::MatrixXd getObservationMatrix() const
    {
      return d_observationMatrix;
    }


  private:
    // DIM x N_v
    Eigen::MatrixXd d_observationMatrix;

  };
}

#endif
