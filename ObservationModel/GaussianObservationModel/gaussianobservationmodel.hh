#pragma once

#include "ObservationModel/observationmodel.hh"

namespace bold {
  
  template<int DIM>
  class GaussianObservationModel : public ObservationModel<DIM>
  {
  public:
    using State = typename ObservationModel<DIM>::State;

    double observationProbGivenState(Eigen::VectorXd const& observation, State const& state) const
    {
      auto expObservation = (*this)(state, observation);
      auto diff = observation - expObservation;

      return
        pow(2.0 * M_PI, -DIM/2) /
        sqrt(d_observationNoiseCovar.determinant()) *
        exp(-0.5 * (diff.transpose() * d_observationNoiseCovar.inverse()).dot(diff));
    }

    void setObservationNoiseCovar(Eigen::MatrixXd const& covar)
    {
      d_observationNoiseCovar = covar;
    }

    Eigen::MatrixXd getObservationNoiseCovar() const
    {
      return d_observationNoiseCovar;
    }

  protected:
    Eigen::MatrixXd d_observationNoiseCovar;
  };

  
}
