#ifndef BOLD_OBSERVATIONMODEL_HH
#define BOLD_OBSERVATIONMODEL_HH

#include <Eigen/Core>

namespace bold
{
  template<int DIM>
  class ObservationModel
  {
  public:
    using State = Eigen::Matrix<double,DIM,1>;

    virtual Eigen::VectorXd operator()(State const& state, Eigen::VectorXd const& observation) const = 0;
  };
}

#endif
