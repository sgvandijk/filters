#pragma once

#include "MotionModel/GaussianMotionModel/gaussianmotionmodel.hh"

#include <cstdlib>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/Cholesky>

namespace bold
{
  template<int DIM>
  class LinearMotionModel : public GaussianMotionModel<DIM>
  {
  public:
    typedef Eigen::Matrix<double,DIM,1> State;

    virtual State operator()(State const& state) const override
    {
      return
        d_stateTransitionModel * state +
        d_controlModel * d_control;
    }


    void setStateTransitionModel(Eigen::MatrixXd const& stateTransitionModel)
    {
      assert(stateTransitionModel.rows() == DIM && stateTransitionModel.cols() == DIM);
      d_stateTransitionModel = stateTransitionModel;
    }

    Eigen::MatrixXd getStateTransitionModel() const
    {
      return d_stateTransitionModel;
    }


    void setControlModel(Eigen::MatrixXd const& controlModel)
    {
      assert(controlModel.rows() == DIM);
      d_controlModel = controlModel;
    }

    Eigen::MatrixXd getControlModel() const
    {
      return d_controlModel;
    }


    void setControl(Eigen::VectorXd const& control)
    {
      assert(control.size() == d_controlModel.cols());
      d_control = control;
    }

    Eigen::VectorXd getControl() const
    {
      return d_control;
    }

  private:

    // DIM x DIM
    Eigen::MatrixXd d_stateTransitionModel;
    // DIM x N_u
    Eigen::MatrixXd d_controlModel;

    Eigen::VectorXd d_control;
  };
}
