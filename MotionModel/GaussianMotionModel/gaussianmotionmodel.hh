#pragma once

#include "MotionModel/motionmodel.hh"
#include <Eigen/Cholesky>
#include <random>

namespace bold {
  
  template<int DIM>
  class GaussianMotionModel : public MotionModel<DIM>
  {
  public:
    GaussianMotionModel()
      : d_normalRNG{std::bind(std::normal_distribution<>(), std::default_random_engine())}
    {
    }

    using State = typename MotionModel<DIM>::State;

    void setProcessNoiseCovar(Eigen::MatrixXd const& covar)
    {
      assert(covar.rows() == DIM && covar.cols() == DIM);
      d_processNoiseCovar = covar;
      d_processNoiseFactor = covar.llt().matrixL();
    }

    Eigen::MatrixXd getProcessNoiseCovar() const
    {
      return d_processNoiseCovar;
    }

    State perturb(State const& state) const override
    {
      Eigen::VectorXd rand(DIM);
      for (unsigned i = 0; i < DIM; ++i)
        rand(i) = d_normalRNG();
      return state + d_processNoiseFactor * rand;
    }

  protected:
    // DIM x DIM
    Eigen::MatrixXd d_processNoiseCovar;
    Eigen::MatrixXd d_processNoiseFactor;

    std::function<double()> d_normalRNG;
  };

}
