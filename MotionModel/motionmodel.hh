#ifndef BOLD_MOTIONMODEL_HH
#define BOLD_MOTIONMODEL_HH

#include <Eigen/Core>

namespace bold
{
  template<int DIM>
  class MotionModel
  {
  public:
    using State = Eigen::Matrix<double,DIM,1>;

    virtual State operator()(State const& state) const = 0;

    virtual State perturb(State const& state) const { return state; } 
  };
}

#endif
