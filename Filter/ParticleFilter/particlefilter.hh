#ifndef BOLD_PARTICLEFILTER_HH
#define BOLD_PARTICLEFILTER_HH

#include "Filter/filter.hh"
#include "ObservationModel/GaussianObservationModel/gaussianobservationmodel.hh"
#include "ObservationModel/GaussianObservationModel/LinearObservationModel/linearobservationmodel.hh"

#include <random>

namespace bold
{
  template<int N>
  struct ESSFun
  {
    static double ESS(Eigen::Matrix<double, N, 1> const& weights)
    {
      return (N / (1.0 + 1.0 / N * ((N * weights).array() - 1).matrix().squaredNorm()));
    }

  };

  /***** ResampleCheck Policies *****/
  template<int DIM, int N, int Threshold>
  struct ESSCheck
  {
    static bool resampleCheck(Eigen::Matrix<double, DIM, N> const& particles,
                              Eigen::Matrix<double, N, 1> const& weights)
    {
      return (ESSFun<N>::ESS(weights) / N) < (Threshold / 100.0);
    }
  };

  template<int DIM, int N>
  using ESSCheck50 = ESSCheck<DIM, N, 50>;

  template<int DIM, int N>
  struct NeverResample
  {
    static bool resampleCheck(Eigen::Matrix<double, DIM, N> const& particles,
                              Eigen::Matrix<double, N, 1> const& weights)
    {
      return false;
    }
  };

  template<int DIM, int N>
  struct AlwaysResample
  {
    static bool resampleCheck(Eigen::Matrix<double, DIM, N> const& particles,
                              Eigen::Matrix<double, N, 1> const& weights)
    {
      return true;
    }
  };

  /***** Resample policies *****/

  /** Systematic Resample
   *
   * Systematic resampling is similar to roulette wheel resampling,
   * however, it requires only a single 'spin', and is linear in nr of
   * particles N. You spin a wheel with N equidistant spokes over a
   * disc with sections of size proportional to the weights. Each
   * spoke selects a particle to sample.
   */
  template<int DIM, int N>
  struct SystematicResample
  {
    typedef Eigen::Matrix<double, DIM, N> ParticleMat;
    typedef Eigen::Matrix<double, N, 1> WeightVec;

    SystematicResample()
      : d_randEng(),
        d_uniDist(0.0, 1.0)
    {}

    std::pair<ParticleMat, WeightVec> resample(ParticleMat const& particles,
                                               WeightVec const& weights,
                                               unsigned nSamples)
    {
      Eigen::Matrix<double, DIM, N> newParticles;

      // Index in old particles
      unsigned i = -1;
      // Spoke index
      unsigned j = 0;
      // Location of first spoke
      double th = d_uniDist(d_randEng) / nSamples;
      // Distance between spokes
      double deltaTh = 1.0 / nSamples;
      // Cumulative sum of spoke distances
      double thSum = th;
      // Cumulative sum over weights
      double wSum = 0;
      while (j < nSamples)
      {
        // Go through particles until we found 
        while (wSum < thSum)
          wSum += weights(++i);
        newParticles.col(j++) = particles.col(i);
        thSum += deltaTh;
      }

      auto newWeights = WeightVec::Constant(weights.size(), 1.0 / N);
      return std::make_pair(newParticles, newWeights);
    }

  protected:
    std::default_random_engine d_randEng;
    std::uniform_real_distribution<double> d_uniDist;
  };


  template<int DIM, int N>
  struct GradualResample : private ESSFun<N>
  {
    typedef Eigen::Matrix<double, DIM, N> ParticleMat;
    typedef Eigen::Matrix<double, N, 1> WeightVec;

    GradualResample()
      : d_randEng(),
        d_uniDist(0.0, 1.0)
    {}

    std::pair<ParticleMat, WeightVec> resample(ParticleMat const& particles,
                                               WeightVec const& weights,
                                               unsigned nSamples)
    {
      auto newParticles = particles;
      auto newWeights = weights;

      unsigned ess = this->ESS(weights);
      unsigned nDead = nSamples < ess ? 0 : nSamples - ess;

      for (unsigned k = 0; k < nDead; ++k)
      {
        // Find a particle to replace
        // Take worst particle
        int worstIdx;
        newWeights.minCoeff(&worstIdx);

        // Find a particle to replace it with
        double th = d_uniDist(d_randEng) * newWeights.sum();
        double wSum = 0;
        int idx = worstIdx;
        for (int i = 0; i < N; ++i)
        {
          wSum += newWeights(i);
          if (wSum >= th)
          {
            idx = i;
            break;
          }
        }

        if (idx == worstIdx)
          continue;
      
        newParticles.col(worstIdx) = newParticles.col(idx);
        newWeights(worstIdx) = newWeights(idx) = newWeights(idx) / 2;
      }

      return std::make_pair(newParticles, newWeights);
    }

  protected:
    std::default_random_engine d_randEng;
    std::uniform_real_distribution<double> d_uniDist;
  };

  /***** Extract policies *****/

  template<int DIM, int N>
  struct MaxWeightExtract
  {
    static std::pair<Eigen::Matrix<double, DIM, 1>, double>
    extractParticle(Eigen::Matrix<double, DIM, N> const& particles,
            Eigen::Matrix<double, N, 1> const& weights)
    {
      unsigned maxWeightIdx;
      weights.maxCoeff(&maxWeightIdx);
      auto bestParticle = particles.col(maxWeightIdx);
      auto diff = Eigen::Matrix<double, DIM, N>{particles.colwise() - bestParticle};
      auto covar = Eigen::Matrix<double, DIM, DIM>{diff * diff.transpose()} / N;
      return std::make_pair(bestParticle, covar.determinant());
    }
  };

  /***** Randomize policies *****/

  template<int DIM, int N>
  struct FunctorRandomState
  {
    std::pair<Eigen::Matrix<double, DIM, 1>, double> getRandomState() const
    {
      return d_stateGenerator();
    }

    void setStateGenerator(std::function<std::pair<Eigen::Matrix<double, DIM, 1>, double>()> const& generator)
    {
      d_stateGenerator = generator;
    }
  protected:
    std::function<std::pair<Eigen::Matrix<double, DIM, 1>, double>()> d_stateGenerator;
  };

  /** Monte Carlo Particle Filter
   */
  template<int DIM, int N,
           template<int,int> class ResampleCheck = ESSCheck50,
           template<int,int> class Resample = SystematicResample,
           template<int,int> class Extract = MaxWeightExtract,
           template<int,int> class RandomState = FunctorRandomState>
  class ParticleFilter :
    public Filter<DIM>,
    public ResampleCheck<DIM,N>,
    public Resample<DIM,N>,
    public Extract<DIM,N>,
    public RandomState<DIM,N>
  {
  public:
    using State = typename Filter<DIM>::State;
    using Particle = std::pair<State,double>;

    ParticleFilter()
      : d_randomizeRatio(0.05)
    {
      reset(State::Zero());
    }

    void reset(State const& state) override
    {
      d_particles.colwise() = state;
      d_weights.fill(1.0 / N);
    }

    void predict(MotionModel<DIM> const& motionModel) override
    {
      // If need to resample
      if (this->resampleCheck(d_particles, d_weights))
      {
        // Resample
        auto nSamples = unsigned(N * (1.0 - d_randomizeRatio));
        std::tie(d_particles, d_weights) = this->resample(d_particles, d_weights, nSamples);

        // Randomize
        for (unsigned i = nSamples; i < N; ++i)
        {
          auto stateWeight = this->getRandomState();
          d_particles.col(i) = stateWeight.first;
          d_weights(i) = stateWeight.second;
        }
      }

      // Do prediction
      for (unsigned i = 0; i < N; ++i)
        d_particles.col(i) = this->d_stateValidator(motionModel.perturb(motionModel(d_particles.col(i))));
    }
    
    void update(ObservationModel<DIM> const& observationModel, Eigen::VectorXd const& observation) override
    {
      auto& gaussianModel = dynamic_cast<GaussianObservationModel<DIM> const&>(observationModel);

      // Update weights
      for (unsigned i = 0; i < N; ++i)
      {
        auto expObservation = observationModel(d_particles.col(i), observation);
        auto diff = this->d_observationDistance(expObservation, observation);

        double p =
          pow(2.0 * M_PI, -DIM/2) /
          sqrt(gaussianModel.getObservationNoiseCovar().determinant()) *
          exp(-0.5 * (diff.transpose() * gaussianModel.getObservationNoiseCovar().inverse()).dot(diff));

        d_weights(i) *= p;
      }
    }

    void endStep() override
    {
      normalize();
    }

    void normalize()
    {
      // Normalize weights
      auto sum = d_weights.sum();
      if (sum < 1e-6)
        d_weights.fill(1.0 / N);
      else
        d_weights /= d_weights.sum();
    }

    std::pair<State,double> extract() const override
    {
      return this->extractParticle(d_particles, d_weights);
    }

    Eigen::Matrix<double, DIM, N> const& getParticles() const { return d_particles; };

    Eigen::Matrix<double, N, 1> const& getWeights() const { return d_weights; };

    void setRandomizeRatio(double ratio)
    {
      d_randomizeRatio = ratio;
    }

    void randomise()
    {
      for (unsigned i = 0; i < N; ++i)
      {
        auto stateWeight = this->getRandomState();
        d_particles.col(i) = stateWeight.first;
        d_weights(i) = stateWeight.second;
      }
    }

    void transform(std::function<Eigen::Matrix<double, DIM, 1>(Eigen::Matrix<double, DIM, 1>)> transform)
    {
      for (unsigned i = 0; i < N; ++i)
        d_particles.col(i) = transform(d_particles.col(i));
    }

    Eigen::Matrix<double, DIM, 1> mean() const
    {
      return d_particles.rowwise().mean();
    }

    Eigen::Matrix<double, DIM, DIM> covar() const
    {
      auto diff = d_particles.colwise() - mean();
      return (diff * diff.transpose()) / N;
    }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  private:
    Eigen::Matrix<double, DIM, N> d_particles;
    Eigen::Matrix<double, N, 1> d_weights;
    double d_randomizeRatio;
  };

  using ParticleFilter6 = ParticleFilter<6,100>;
  using ParticleFilter3 = ParticleFilter<3,100>;
}

#endif
