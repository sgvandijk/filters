#ifndef BOLD_FILTER_HH
#define BOLD_FILTER_HH

#include "MotionModel/motionmodel.hh"
#include "ObservationModel/observationmodel.hh"

#include <Eigen/Core>
#include <functional>
#include <utility>

namespace bold
{
  template<int DIM>
  class Filter
  {
  public:
    using State = Eigen::Matrix<double,DIM,1>;

    Filter()
      : d_stateValidator{[](State const& state) { return state; }},
      d_stateDistance{[](State const& a, State const& b) { return b - a; }},
      d_observationDistance{[](Eigen::VectorXd const& a, Eigen::VectorXd const& b) { return b - a; }}
    {}

    void setStateValidator(std::function<State(State const&)> validator)
    {
      d_stateValidator = validator;
    }

    void setStateDistance(std::function<State(State const&, State const&)> distance)
    {
      d_stateDistance = distance;
    }

    void setObservationDistance(std::function<Eigen::VectorXd(Eigen::VectorXd const&, Eigen::VectorXd const&)> distance)
    {
      d_observationDistance = distance;
    }

    /** Set state estimate
     *
     * @param state Most likely state to replace current estimate with
     */
    virtual void reset(State const& state) = 0;

    /** Predict state based on motion model
     *
     * @param motionModel A function that returns a state prediction
     * based on a given previous state.
     */
    virtual void predict(MotionModel<DIM> const& motionModel) = 0;

    /** Update state based on observations
     *
     * @param observationModel A function that returns the most likely
     * expected observation vector given a state and an actual
     * observation.
     */
    virtual void update(ObservationModel<DIM> const& observationModel, Eigen::VectorXd const& observation) = 0;

    virtual std::pair<State,double> extract() const = 0;

    virtual void endStep() {}

  protected:
    std::function<State(State const&)> d_stateValidator;
    std::function<State(State const&, State const&)> d_stateDistance;
    std::function<Eigen::VectorXd(Eigen::VectorXd const&, Eigen::VectorXd const&)> d_observationDistance;

  };
}

#endif
