#ifndef BOLD_KALMANFILTER_HH
#define BOLD_KALMANFILTER_HH

#include "Filter/filter.hh"

#include "MotionModel/GaussianMotionModel/LinearMotionModel/linearmotionmodel.hh"
#include "ObservationModel/GaussianObservationModel/LinearObservationModel/linearobservationmodel.hh"

namespace bold
{
  template<int DIM>
  class KalmanFilter : public Filter<DIM>
  {
  public:
    using State = typename Filter<DIM>::State;

    virtual void reset(State const& state) override
    {
      d_state = state;
      d_stateCovar = Eigen::Matrix<double,DIM,DIM>::Identity();
    }

    virtual void predict(MotionModel<DIM> const& motionModel) override
    {
      auto& linearModel = dynamic_cast<LinearMotionModel<DIM> const&>(motionModel);

      d_state = this->d_stateValidator(linearModel.getStateTransitionModel() * d_state + 
                                       linearModel.getControlModel() * linearModel.getControl());
        
      d_stateCovar =
        linearModel.getStateTransitionModel() *
        d_stateCovar *
        linearModel.getStateTransitionModel().transpose() +
        linearModel.getProcessNoiseCovar();
    }

    virtual void update(ObservationModel<DIM> const& observationModel, Eigen::VectorXd const& observation) override
    {
      auto& linearModel = dynamic_cast<LinearObservationModel<DIM> const&>(observationModel);

      Eigen::VectorXd z = observation;
      Eigen::MatrixXd H = linearModel.getObservationMatrix();

      Eigen::VectorXd residual = this->d_observationDistance(linearModel(d_state, observation), z);
      Eigen::MatrixXd residualCovar =
        H * d_stateCovar * H.transpose() +
        linearModel.getObservationNoiseCovar();
      Eigen::MatrixXd gain = d_stateCovar * H.transpose() * residualCovar.inverse();
      d_state = this->d_stateValidator(d_state + gain * residual);
      d_stateCovar = (Eigen::MatrixXd::Identity(DIM, DIM) - gain * H) * d_stateCovar;
    }

    virtual std::pair<State, double> extract() const override
    {
      return std::pair<State, double>(d_state, d_stateCovar.determinant());
    }

    Eigen::MatrixXd getStateCovar() const
    {
      return d_stateCovar;
    }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  private:
    State d_state;
    Eigen::MatrixXd d_stateCovar;

  };
}

#endif
