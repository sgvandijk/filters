#pragma once

#include "Filter/filter.hh"
#include "MotionModel/GaussianMotionModel/gaussianmotionmodel.hh"
#include "ObservationModel/GaussianObservationModel/gaussianobservationmodel.hh"

#include <Eigen/Cholesky>

namespace bold
{
  template <int DIM>
  class UnscentedKalmanFilter : public Filter<DIM>
  {
  public:
    typedef Eigen::Matrix<double,DIM,1> State;

    virtual void reset(State const& state) override
    {
      d_state = state;
      d_stateCovar = Eigen::Matrix<double,DIM,DIM>::Identity();
    }

    virtual void predict(MotionModel<DIM> const& motionModel) override
    {
      static unsigned constexpr nSigmaPoints = 2 * DIM + 1;
      
      auto& gaussianModel = dynamic_cast<GaussianMotionModel<DIM> const&>(motionModel);

      std::tie(d_sigmaPoints, d_weights) = getSigmaPoints(d_state, d_stateCovar, 1.0 / nSigmaPoints);

      // Propagate sigma points through the motion model
      for (unsigned i = 0; i < d_sigmaPoints.cols(); ++i)
        d_sigmaPoints.col(i) = this->d_stateValidator(motionModel(d_sigmaPoints.col(i)));

      // Determine predicted distribution
      d_predictedState = d_sigmaPoints * d_weights;

      auto delta = Eigen::MatrixXd{d_sigmaPoints.rows(), d_sigmaPoints.cols()};
      for (unsigned i = 0; i < d_sigmaPoints.cols(); ++i)
        delta.col(i) = this->d_stateDistance(d_predictedState, d_sigmaPoints.col(i));

      d_predictedCovar = gaussianModel.getProcessNoiseCovar();
      for (unsigned i = 0; i < d_weights.size(); ++i)
        d_predictedCovar += d_weights(i) * delta.col(i) * delta.col(i).transpose();
    }

    virtual void update(ObservationModel<DIM> const& observationModel, Eigen::VectorXd const& observation) override
    {
      static unsigned constexpr nSigmaPoints = 2 * DIM + 1;
      
      auto& gaussianModel = dynamic_cast<GaussianObservationModel<DIM> const&>(observationModel);

      auto Z = Eigen::MatrixXd(observation.size(), d_sigmaPoints.cols());
      for (unsigned i = 0; i < Z.cols(); ++i)
        Z.col(i) = observationModel(d_sigmaPoints.col(i), observation);

      Eigen::VectorXd zMean = Z * d_weights;

      auto delta = Eigen::MatrixXd{Z.rows(), Z.cols()};
      for (unsigned i = 0; i < Z.cols(); ++i)
        delta.col(i) = this->d_observationDistance(zMean, Z.col(i));

      auto S = gaussianModel.getObservationNoiseCovar();
      for (unsigned i = 0; i < d_weights.size(); ++i)
        S += d_weights(i) * delta.col(i) * delta.col(i).transpose();

      // TODO: should be able to just use predicted sigma points; this way is
      // nicer in theory as the prediction step may be done using
      // traditional KF
      Eigen::MatrixXd predictedSigmaPoints;
      Eigen::VectorXd predictedWeights;
      std::tie(predictedSigmaPoints, predictedWeights) = getSigmaPoints(d_predictedState, d_predictedCovar, 1.0 / nSigmaPoints);

      Eigen::MatrixXd P = Eigen::MatrixXd::Zero(DIM, observation.size());
      for (unsigned i = 0; i < predictedWeights.size(); ++i)
        P += d_weights[i] * 
          this->d_stateDistance(d_predictedState, predictedSigmaPoints.col(i)) * 
          this->d_observationDistance(zMean, Z.col(i)).transpose();

      auto K = Eigen::MatrixXd{P * S.inverse()};
      d_state = d_predictedState + K * (observation - zMean);
      d_stateCovar = d_predictedCovar - K * S * K.transpose();
    }

    virtual std::pair<State, double> extract() const override
    {
      return std::pair<State, double>(d_state, d_stateCovar.determinant());
    }

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  private:

    State d_state;
    Eigen::MatrixXd d_stateCovar;

    Eigen::VectorXd d_weights;
    Eigen::MatrixXd d_sigmaPoints;

    State d_predictedState;
    Eigen::MatrixXd d_predictedCovar;

    std::pair<Eigen::MatrixXd,Eigen::VectorXd> getSigmaPoints(State const& mu, Eigen::MatrixXd const& covar, double w0)
    {
      static unsigned constexpr nSigmaPoints = 2 * DIM + 1;
      
      Eigen::MatrixXd sigmaPoints(DIM, nSigmaPoints);
      Eigen::VectorXd weights{nSigmaPoints};

      // Central weight selection
      // Tutorial: -1 < W0 < 1
      // Wikipedia: W0 = .\ / (L + .\) where L = DIM
      // .\ = 1 --> all weights equal?

      weights(0) = w0;
      weights.tail<nSigmaPoints - 1>().fill((1.0 - weights(0)) / (2.0 * DIM));

      sigmaPoints.col(0) = mu;

      auto P = double{DIM} / (1.0 - weights(0)) * covar;
      Eigen::MatrixXd Psqrt = P.llt().matrixL();

      for (unsigned i = 1; i <= DIM; ++i)
      {
        sigmaPoints.col(i) = sigmaPoints.col(0) + Psqrt.col(i - 1);
        sigmaPoints.col(i + DIM) = sigmaPoints.col(0) - Psqrt.col(i - 1);
      }

      return std::make_pair(sigmaPoints, weights);
    }
  };
}
