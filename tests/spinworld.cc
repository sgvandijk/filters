#include "Filter/KalmanFilter/kalmanfilter.hh"
#include "Filter/UnscentedKalmanFilter/unscentedkalmanfilter.hh"
#include "Filter/ParticleFilter/particlefilter.hh"

#include "MotionModel/GaussianMotionModel/LinearMotionModel/linearmotionmodel.hh"
#include "ObservationModel/GaussianObservationModel/LinearObservationModel/linearobservationmodel.hh"

#include <memory>
#include <iostream>
#include <chrono>
#include <Eigen/Geometry>

using namespace bold;
using namespace std;
using namespace Eigen;

#define DIM 2

const unsigned nEpochs{1000};

void run(unique_ptr<Filter<DIM>> filter, Filter<DIM>::State state);
Filter<DIM>::State complexProd(Filter<DIM>::State const& a, Filter<DIM>::State const& b);
double compToAngle(Filter<DIM>::State const c);
Filter<DIM>::State angleToComp(double theta);

int main(int argc, char** argv)
{
  char filterToUse = 'k';
  for (unsigned i = 1; i < argc; ++i)
    filterToUse = argv[i][1];

  unique_ptr<Filter<DIM>> filter;
  if (filterToUse == 'k')
    filter.reset(new KalmanFilter<DIM>{});
  else if (filterToUse == 'u')
  {
    filter.reset(new UnscentedKalmanFilter<DIM>{});
  }
  else if (filterToUse == 'p')
  {
    auto particleFilter = unique_ptr<ParticleFilter<DIM,50>>{new ParticleFilter<DIM,50>{}};
    auto stateDist = uniform_real_distribution<double>{-1.0, 1.0};
    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    function<double()> stateRng = bind(stateDist, default_random_engine(seed));
    
    particleFilter->setStateGenerator([stateRng]() { return make_pair(Filter<DIM>::State::Ones() * stateRng(), 0.01); });
    filter = move(particleFilter);
  }

  auto stateValidator = [](Filter<DIM>::State const& state) {
      Filter<DIM>::State validState;
      validState = state / state.norm();
      return validState;
  };

  auto startState = stateValidator(Filter<DIM>::State::Random());
  filter->reset(stateValidator(Filter<DIM>::State::Random()));

  filter->setStateValidator(stateValidator);

  run(move(filter), startState);
}

void run(unique_ptr<Filter<DIM>> filter, Filter<DIM>::State state)
{
  auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();

  auto processNoiseStdDev = 0.02;
  auto observationNoiseStdDev = 0.2;

  auto stateValidator = [](Filter<DIM>::State const& state) {
      Filter<DIM>::State validState;
      validState = state / state.norm();
      return validState;
  };

  auto motionModel = LinearMotionModel<DIM>{};
  motionModel.setStateTransitionModel(Rotation2D<double>{0.1 * M_PI}.matrix());
  motionModel.setControlModel(MatrixXd::Zero(DIM,1));
  motionModel.setControl(VectorXd::Zero(1));
  motionModel.setProcessNoiseCovar(MatrixXd::Identity(DIM, DIM) * processNoiseStdDev * processNoiseStdDev);

  auto observationModel = LinearObservationModel<DIM>{};
  observationModel.setObservationMatrix(MatrixXd::Identity(DIM, DIM));
  observationModel.setObservationNoiseCovar(MatrixXd::Identity(DIM, DIM) * observationNoiseStdDev * observationNoiseStdDev);

  auto observationNoiseDist = normal_distribution<double>{0, observationNoiseStdDev};
  auto observationNoise = bind(observationNoiseDist, default_random_engine{seed});

  for (unsigned e = 0; e < nEpochs; ++e)
  {
    Filter<DIM>::State stateEst;
    double stateEstCovar;
    tie(stateEst, stateEstCovar) = filter->extract();

    cout << e << "\t" << state.transpose() << " " << stateEst.transpose()  << " " << stateEstCovar << endl;

    state = stateValidator(motionModel(state));

    filter->predict(motionModel);
    
    auto observation = observationModel(state, Filter<DIM>::State{});

    observation(0) += observationNoise();
    observation(1) += observationNoise();
    observation = stateValidator(observation);

    filter->update(observationModel, observation);
    
    filter->endStep();
  }
}

Filter<DIM>::State complexProd(Filter<DIM>::State const& a, Filter<DIM>::State const& b)
{
  Filter<DIM>::State res;
  res(0) = a(0) * b(0) - a(1) * b(1);
  res(1) = a(0) * b(1) + a(1) * b(0);
  return res;
}

double compToAngle(Filter<DIM>::State const c)
{
  return atan2(c(1), c(0));
}

Filter<DIM>::State angleToComp(double theta)
{
  return Filter<DIM>::State{cos(theta), sin(theta)};
}
