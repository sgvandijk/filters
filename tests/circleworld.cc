#include "Filter/KalmanFilter/kalmanfilter.hh"
#include "Filter/UnscentedKalmanFilter/unscentedkalmanfilter.hh"
#include "Filter/ParticleFilter/particlefilter.hh"

#include "MotionModel/GaussianMotionModel/LinearMotionModel/linearmotionmodel.hh"
#include "ObservationModel/GaussianObservationModel/LinearObservationModel/linearobservationmodel.hh"

#include <memory>
#include <iostream>
#include <chrono>

using namespace bold;
using namespace std;
using namespace Eigen;

const unsigned nEpochs{1000};

void run(unique_ptr<Filter<1>>, Filter<1>::State);

double circularClamp(double x);

int main(int argc, char** argv)
{
  char filterToUse = 'k';
  for (unsigned i = 1; i < argc; ++i)
    filterToUse = argv[i][1];

  unique_ptr<Filter<1>> filter;
  if (filterToUse == 'k')
    filter.reset(new KalmanFilter<1>{});
  else if (filterToUse == 'u')
  {
    filter.reset(new UnscentedKalmanFilter<1>{});
  }
  else if (filterToUse == 'p')
  {
    auto particleFilter = unique_ptr<ParticleFilter<1,50>>{new ParticleFilter<1,50>{}};
    auto stateDist = uniform_real_distribution<double>{-1.0, 1.0};
    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();
    function<double()> stateRng = bind(stateDist, default_random_engine(seed));
    
    particleFilter->setStateGenerator([stateRng]() { return make_pair(Filter<1>::State::Ones() * stateRng(), 0.01); });
    filter = move(particleFilter);
  }

  auto startState = Filter<1>::State::Zero();
  filter->reset(Filter<1>::State::Random());

  auto distance = [](VectorXd const& z1, VectorXd const& z2) {
      auto d = VectorXd{z2 - z1};
      if (d(0) < -1.0)
        d(0) = 2.0 + d(0);
      if (d(0) > 1.0)
        d(0) = -2.0 + d(0);
      return d;
  };


  filter->setStateValidator([](Filter<1>::State const& state) {
      Filter<1>::State validState;
      validState(0) = circularClamp(state(0));
      return validState;
    });

  filter->setStateDistance(distance);
  filter->setObservationDistance(distance);

  run(move(filter), startState);
}

void run(unique_ptr<Filter<1>> filter, Filter<1>::State state)
{
  auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();

  auto processNoiseMean = 0.0;
  auto processNoiseStdDev = 0.05;

  auto observationNoiseMean = 0.0;
  auto observationNoiseStdDev = 0.05;

  auto processNoiseDist = normal_distribution<double>{processNoiseMean, processNoiseStdDev};
  auto processNoise = bind(processNoiseDist, default_random_engine{seed});

  auto observationNoiseDist = normal_distribution<double>{observationNoiseMean, observationNoiseStdDev};
  auto observationNoise = bind(observationNoiseDist, default_random_engine{seed});

  auto motionModel = LinearMotionModel<1>{};
  motionModel.setStateTransitionModel(MatrixXd::Identity(1,1));
  motionModel.setControlModel(MatrixXd::Identity(1,1));
  motionModel.setProcessNoiseCovar(MatrixXd::Identity(1,1) * processNoiseStdDev * processNoiseStdDev);

  auto observationModel = LinearObservationModel<1>{};
  observationModel.setObservationMatrix(MatrixXd::Identity(1,1));
  observationModel.setObservationNoiseCovar(MatrixXd::Identity(1,1) * observationNoiseStdDev * observationNoiseStdDev);

  VectorXd observation = VectorXd::Zero(1);

  for (unsigned e = 0; e < nEpochs; ++e)
  {
    Filter<1>::State stateEst;
    double stateEstCovar;
    tie(stateEst, stateEstCovar) = filter->extract();

    cout << e << "\t" << state(0) << " " << observation(0) << " " << stateEst(0) << " " << stateEstCovar << endl;
    // Update state
    auto control = 0.01 * VectorXd::Ones(1);
    state += control;
    state.array() += processNoise();
    state(0) = circularClamp(state(0));

    // Do prediction
    motionModel.setControl(control);
    filter->predict(motionModel);

    // Create observation
    observation = VectorXd{state.array() + observationNoise()};
    observation(0) = circularClamp(observation(0));

    // Do update
    filter->update(observationModel, observation);

    filter->endStep();
  }
}

double circularClamp(double x)
{
  while (x < -1.0)
    x += 2.0;
  while (x > 1.0)
    x -= 2.0;
  return x;
}
